from dzien12.pracownik import Pracownik


prac1 = Pracownik("Jan", "Kowalski")
prac1.wypisz_imie()

# stanowisko jest property czyli korzystam z niego jak ze zmiennej!!!!
# czyli po kropce podaję nazwę, ale NIE UŻYWAM NAWIASÓW
print(prac1.stanowisko)

# do property informacje wprowadzam jak do zmiennej czyli
# korzystam ze znaku przypisania
prac1.stanowisko = "mechanik"
print(prac1.stanowisko)

prac1.pensja = 6000
print("Mechanik zarabia {}".format(prac1.pensja))
