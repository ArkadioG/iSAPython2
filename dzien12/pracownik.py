class Pracownik(object):
    """Definiuje informacje i funkcje dla pracownika"""

    liczba_pracownikow = 0
    roczna_podwyzka = 5

    def __init__(self, imie, nazwisko):
        """Inicjalizuje instancję klasy Pracownik
        (str, str) -> Pracownik
        """
        # poniższe zmienne są prywatne, więc jeśli chcemy
        # nimi manipulować to musimy udostępnić metody lub propertisy
        self.__imie = imie
        self.__nazwisko = nazwisko
        self.__stanowisko = None
        self.__wynagrodzenie = None
        Pracownik.liczba_pracownikow += 1

    def wypisz_imie(self):
        """Wypisuje imie pracownika"""
        print(self.__imie)

    def oblicz_podwyzke(self):
        """Oblicza wartość rocznej podwyżki"""
        return self.__wynagrodzenie * (Pracownik.roczna_podwyzka / 100)

    # to jest getter - służy do pobierania wartości ze zmiennej prywatnej
    # w getterze kontrolujemy w jaki sposób informacja jest przekazywana
    # gettera tworzymy używając dekoratora @property i dalej definiujemy jak funkcję
    # ale używamy jak zmienną!!!
    @property
    def stanowisko(self):
        """Zwraca nazwę stanowiska"""
        if self.__stanowisko is not None:
            return self.__stanowisko.capitalize()
        else:
            return "Nieokreślone"

    # to jest setter, służy do wpisywania informacji do prywatnej zmiennej
    # w kontrolowany przez nas sposób, settera tworzymy
    # używając dekoratora @nazwa_gettera.setter, dalej definiujemy jak
    # metodę z tą sama nazwą jak getter.
    # używamy jak zmienną - czyli przypisujemy jakąś wartość do property
    @stanowisko.setter
    def stanowisko(self, nazwa):
        """Zapisuje nazwę stanowiska"""
        if str(nazwa).isalpha():
            self.__stanowisko = nazwa

    @property
    def pensja(self):
        """Zwraca wysokość pensji pracownika"""
        return self.__wynagrodzenie

    @pensja.setter
    def pensja(self, kwota):
        """
        Ustawia wysokość pensji pracownika
        """
        if kwota <= 10000:
            self.__wynagrodzenie = kwota
        else:
            self.__wynagrodzenie = 10000

    def __del__(self):
        Pracownik.liczba_pracownikow -= 1

    def __str__(self):
        return "{} {} stanowisko: {}".format(self.__imie, self.__nazwisko, self.__stanowisko)
