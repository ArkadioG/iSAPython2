class Zawodnik(object):
    """Klasa Zawodnik - zawiera informacje i metody odpowiednie dla zawodnika"""

    def __init__(self, imie, dyscyplina):
        """Inicjalizuje instancję klasy Zawodnik
        (str, str) -> Zawodnik"""
        self.name = imie
        self.dyscyplina = dyscyplina

        # to jest prywatna zmienna instancji
        # jej nazwa zaczyna się od dwóch podkreślników
        # taka zmienna jest możliwa do użycia w kodzie klasy Zawodnik
        # nie ma do niej dostępu poza tą klasą
        self.__numer_koszulki = None

    # ta metoda statyczna jest prywatna
    # nazwa zaczyna się od dwóch podkreślników
    # metodę tą można użyć tylko wewnątrz klasy
    # będzie ona niewidoczna poza klasą Zawodnik
    @staticmethod
    def __sprawdz_numer(numer):
        """Ma sprawdzać czy numer koszulki jest poprawny"""
        if numer < 0 or numer > 99:
            return False
        else:
            return True

    def zmien_numer(self, nowy_numer):
        # metoda prywadna jest widoczna w klasie
        if not Zawodnik.__sprawdz_numer(nowy_numer):
            print("Niewłaściwy numer")
        else:
            # zmienna prywatna jest widoczna w klasie
            self.__numer_koszulki = nowy_numer

    def daj_numer_koszulki(self):
        return self.__numer_koszulki


koszykarz = Zawodnik("Michael Jordan", "Koszykówka")
koszykarz.zmien_numer(23)

# zmiennej prywatnej nie można użyć poza klasą
# próba uruchomienia poniższego printa spowoduje błąd
# print(koszykarz.__numer_koszulki)

# do manipulowania prywatnymi zmiennymi
# musimy korzystać z dostępnych metod (jeśli są)
print(koszykarz.name, koszykarz.daj_numer_koszulki())

# zmienna prywatna jest ukryta pod zmienioną nazwą
koszykarz._Zawodnik__numer_koszulki = 45

print(koszykarz.daj_numer_koszulki())

pilkarz = Zawodnik("Robert Lewandowski", "Nożna")
pilkarz.zmien_numer(123)

# uzywając poniższego polecenia podejrzymy zawartości klas i obiektów
# korzystamy z __dict__
print(pilkarz.__dict__)
print()

print(Zawodnik.__dict__)
print()
print(koszykarz.__dict__)
