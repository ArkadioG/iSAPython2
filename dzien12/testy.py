from unittest import TestCase
from dzien12.pracownik import Pracownik

from contextlib import redirect_stdout
import io

class TestPracownik(TestCase):

    @staticmethod
    def get_print_output(testowana_funkcja):
        """FUnkcja pomocnicza, przechwytuje standardowy output, inaczej
        mówiąc - przechwytuje to co miało zostać wypisane na ekranie"""

        # tworzymy bufor w pamięci na przechwycone dane
        bufor_pamieci = io.StringIO()

        # przekierowujemy standardowy output do buforu
        # czyli przechwytujemy działanie print()
        with redirect_stdout(bufor_pamieci):
            # uruchamiamy funkcję, z której print() przechwytujemy
            testowana_funkcja()
            # zwracany przechwyconą wartość
            return bufor_pamieci.getvalue()

    # metoda setUp jest orzez nas nadpisywana - ta metoda
    # uruchamiana jest przed każdym testem
    # wykorzystujemy ją do inicjalizowania potrzebnych informacji
    # które są wspólne dla wszystkich metod
    def setUp(self):
        """Inicjalizuje test"""
        self.prac = Pracownik("jan", "kowalski")
        self.prac.pensja = 2000
        self.prac.stanowisko = "mechanik"

    def test_oblicz_podwyzke(self):
        """Test dla metody oblicz_podwyzke"""
        # wykonuje testowaną metodę, dane potrzebne mam zainicjalizowane
        # w setUp
        wart_podwyzki = self.prac.oblicz_podwyzke()
        # moja wartość oczekiwana
        wart_oczekiwana = 100
        # sprawdzam czy wartość zwrocona przez testowaną metodę odpowiada
        # wartości oczekiwanej
        self.assertEqual(wart_podwyzki, wart_oczekiwana)

    def test_stanowisko_get(self):
        """Test gettera stanowisko"""
        # wartość oczekiwana
        oczek_stan = "Mechanik"
        # uruchamiam gettera
        stan = self.prac.stanowisko
        # sprawdzam wartość zwrócona przez gettera z oczekiwaną
        self.assertEqual(stan, oczek_stan)

    def test_wypisz_imie(self):
        """Test dla metody wypisz_imie
        Metoda ta używa printa a nie return, muszę więc
        użyć helpera aby przechwycić działanie print()"""

        # testowana metoda - przypisuje do zmiennej (nie musze)
        metoda_testowana = self.prac.wypisz_imie

        # używam helpera do wywołania testowanej metody i przechwycenia print()
        rzeczyw_output = TestPracownik.get_print_output(metoda_testowana)

        # oczekiwany output - pamiętam o znaku nowej linii dodawanym przez printa!!!!
        oczekiwany_output = "jan\n"

        # sprawdzam wynik funkcji z wartością oczekiwaną
        self.assertEqual(rzeczyw_output, oczekiwany_output)
