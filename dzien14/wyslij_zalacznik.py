import os
from dzien13.poczta_zalacznik import Poczta
from dzien13.secrets import *

poczta = Poczta(moj_login,moje_haslo)

adresaci = [moj_login]
temat = "Hello form Arek - załączniki"
tresc = "To są załączniki"

# zalaczniki = ["c:\\wyslac\\babciakorale.png",
#               "c:\\wyslac\\babciawild150.png",
#               "c:\\wyslac\\autumn_woman150.png"]

root = "c:\\wyslac"
# zalaczniki = [os.path.join(root, plik) for plik in os.listdir(root)]
# print(zalaczniki)

zalacz = []
for plik in os.listdir(root):
    pelna_sciezka = os.path.join(root, plik)
    zalacz.append(pelna_sciezka)


poczta.wyslij_wiadomosc(adresaci, temat, tresc, zalacz)
