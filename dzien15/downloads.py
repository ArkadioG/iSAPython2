import requests

def pobierz_foto(link, lokalizacja):
    """link - adres obrazka w sieci
        lokalizacja - ścieżka na dysku i nazwa pliku"""

    odpowiedz = requests.get(link)

    if odpowiedz.status_code == 200:
        # licznik zapisanych bajtów
        bajty = 0
        try:
            with open(lokalizacja, "wb") as plik:
                for kawalek in odpowiedz.iter_content(100000):
                    ilosc = plik.write(kawalek)
                    bajty += ilosc
        except FileNotFoundError:
            # tu powinna być implementacja tworząca folder i plik
            print("tworze folder i zapisuje plik")
            bajty = 23423

        print(bajty)