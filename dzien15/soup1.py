import requests
from bs4 import BeautifulSoup

# pobieramy stronę i zapisujemy ją do zmiennej
response = requests.get("http://trojmiasto.pl")

# otrzymana odpowiedź ma pole statusu
# status jest 200 jeśli wszystko było ok i serwer
# oddał nam żądaną stronę
print(response.status_code)
# response.raise_for_status()
if response.status_code == 200:
    with open("trojmiasto.txt", "w", encoding="utf-8") as plik:
        plik.write(response.text)

trojmiasto_soup = BeautifulSoup(response.text, "html.parser")

linki = trojmiasto_soup.select(".news-list li a")
# print(linki)

for link in linki:
    print(link.getText())
    print(str(link))
    # print(link.attrs)
    print(link.get('id'))
    print(link.get('title'), link.get('href'))
